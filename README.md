# Wordpress usando Zero Trust do Cloudflare

Nesse trabalho estou usando o Docker Compose para criar um servidor isolado de Wordpress, MariaDb e Cloudflare para prover o serviço via Zero Trust. 

O Bitname é a base desses serviços.

Ele pode ser executado em qualquer máquina, mesmo isolada. Pois o serviço do Cloudflare se conecta aos sistemas do Cloudflare. E de lá, ele provê os serviços Web.

# Instalação
Para fazer o Docker Compose funcionar, você deverá acrescentar realizar os seguintes comandos:

## Clonagem

Para clonar ao seu próprio site, você pode fazer da seguinte forma:

```bash
git clone https://gitlab.com/dioclecio/servidor-podman-cloudflare-wordpress.git <nome-da-pasta>
```

Esse <nome-da-pasta> será o nome do serviço conforme ele deve ser usado em cada máquina. Se você for utilizar mais de um serviço como este, sugiro utilizar nomes descritivos. Ex: worpress01, wordpress02, wordpress03

## Crie seu próprio domínio no Cloudflare
Nessa parte, você deverá colocar um domínio e fazer um tunel. 
Esse site explica um passo a passo.

[Domínio e Cloudflare](https://noted.lol/cloudflare-tunnel-and-zero-trust/)

Você deve copiar a chave do Tunel para a variável <tunnelToken> , conforme mostra abaixo.

![image-20240310150620243](./img/image-20240310150620243.png)

Após copiado o Token, você deve configurar o site adotando os seguintes pontos:

![image-20240310150819199](./img/image-20240310150819199.png)

1. O subdomínio: Ex. teste-site01
2. O domínio que está hospedado no Cloudflare
3. O tipo de protocolo. Nesse caso, adotaremos http.
4. E a URL apontada dentro do docker. No presente caso, adotamos **wp:8080**.

Após definir, nosso site deverá funcionar no endereço tese-site01.dioclecio.com

Por padrão, esse endereço vai configurado com SSL.

## Arquivo de variáveis

Crie um arquivo .env na mesma pasta com as seguintes variáveis. Utilize a referência deixada pelo arquivo <exemplo.env>. 

```
dbUser="seu usuario de banco"
dbPassword="sua senha"
dbRootPassword="senha do Root"
tunnelToken="Token Fornecido pelo Cloudflare"

```

## Execução

Esse docker pode ser executado em qualquer servidor. Se você estiver executando em alguma Máquina Virtual, poderia sugerir o Fedora CoreOS. Nele você terá mais facilidade para implementar e evitar questões como permissões. 

Para servidores como CentOS e Alma Linux, que utilizam o SELinx, você deverá fazer alguns ajustes nas permissões.

Para executar você deve entrar na pasta e digitar os seguinte comandos:

```
# cd <exemplo-de-pasta>
# podman-compose up -d

```

## Pastas de persistência

Os containers estão programados para ter sua própria persistência. Para copiar para as pasta, você deve utilizar o <podman unshare>.

As pastas de persistência somente ficam disponíveis depois da execução. 

```bash
# podman volume ls
DRIVER      VOLUME NAME
local       site-aahu_vol-db
local       site-aahu_vol-wp

# podman unshare
# podman volume mount site-aahu_vol-wp
/home/dioclecio/.local/share/containers/storage/volumes/site-aahu_vol-wp/_data

# cp -rf dados/* /home/dioclecio/.local/share/containers/storage/volumes/site-aahu_vol-wp/_data
# exit 
```

## Entrada no serviço do Wordpress

Para configurar o Wordpress, basta visitar o endereço e em seguida a subpasta </wp-admin>. 

Ex: https://site-teste01.dioclecio.com/wp-admin

O usuário e senha padrão são:

- Username: user
- Password: bitnami

![image-20240310151254554](./img/image-20240310151254554.png)

